import { AgGridReact } from 'ag-grid-react'
import React, { FC, useMemo, useRef } from 'react'
import { columnDefs } from '../definitions/ColDefs'
import { ITableGrid } from '../interfaces/ITableGrid';

const TableGrid:FC<ITableGrid> = ({data}) => {
  const gridRef = useRef<AgGridReact>(null);
  const defaultColDef = useMemo(() => ({
    sortable: true,
    filter: true,
    resizable: true,
    flex: 1,
    minWidth: 100,
    editable: true,
  }), []);
  
  return (
    <AgGridReact
    ref={gridRef}
    defaultColDef={defaultColDef}
    rowData={data}
    columnDefs={columnDefs}
    rowGroupPanelShow={'always'}
    pagination={true}
    paginationPageSize={50}
    animateRows={true}
    enableCharts={true}
    enableCellTextSelection={true}
    enableCellChangeFlash={true}
  />
  )
}

export default TableGrid
