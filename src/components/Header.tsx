import { FC } from 'react'
import { IHeader } from '../interfaces/IHeader'


const Header: FC<IHeader> = ({ onAddRange, onClearRange, onResetFilters }) => {
  return (
    <>
      <h1>Near-Earth Object Overview</h1>
      {/* <div className="example-wrapper">
        <div className="example-header">
          <button onClick={onAddRange}>Add Range</button>
          <button onClick={onClearRange}>Clear Range</button>
          <button onClick={onResetFilters}>Reset Filters</button>
        </div>
      </div> */}
    </>
  )
}

export default Header