export interface IHeader {
  onAddRange?: () => void
  onClearRange?: () => void
  onResetFilters?: () => void
}