export interface Data {
  discovery_date?: string;
  pha?: string;
  designation?: string;
  h_mag?: string;
  moid_au?: string;
  q_au_1?: string;
  q_au_2?: string;
  period_yr?: string;
  i_deg: string;
  orbit_class?: string;
}

export interface ITableGrid {
  data: Data[];
}