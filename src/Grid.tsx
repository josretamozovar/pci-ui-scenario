import data from "./near-earth-asteroids.json";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-alpine.css";
import { useMemo } from "react";
import Header from "./components/Header";

import TableGrid from "./components/TableGrid";
import { Data } from './interfaces/ITableGrid';

const NeoGrid = (): JSX.Element => {

  const containerStyle = useMemo(() => ({ width: '100%', height: '100%' }), []);
  const new_data: Data[] = data.map(e => (
    {
      ...e,
      discovery_date: new Date(e.discovery_date).toDateString(),
      pha: e.pha === "Y" ? "Yes" : e.pha === "N" ? "No" : "",
    }
  ));

  // *TODO : add range selection, clear range selection, reset filters

  // const onClearRange = useCallback(() => {
  //   gridRef?.current?.api.clearRangeSelection();
  // }, []);

  // //clear filters react data grid
  // const onAddRange = useCallback(() => {
  //   gridRef?.current?.api.addRangeSelection({
  //     rowStartIndex: 0,
  //     rowEndIndex: 10,
  //     columns: ["designation", "discovery_date", "h_mag", "moid_au", "q_au_1", "q_au_2", "period_yr", "i_deg", "pha", "orbit_class"],
  //   });
  // }, []);

  // // reset filters react data grid
  // const onResetFilters = useCallback(() => {
  //   gridRef?.current?.api.setFilterModel(null);
  // }, []);

  return (
    <div style={containerStyle}>
      <Header />
      <div className="ag-theme-alpine" style={{ height: 900, width: 1920 }}>
        <TableGrid data={new_data} />
      </div>
    </div>
  );
};

export default NeoGrid;
